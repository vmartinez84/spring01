package com.vmartinez.ioc;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsoEmpleados {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Empleado empleado01 = new DirectorEmpleado();
		
		//System.out.println(empleado01.getTareas());
		
		ClassPathXmlApplicationContext contexto = new ClassPathXmlApplicationContext("applicationContext.xml");
		Empleado empleado = contexto.getBean("MiEmpleado", Empleado.class);
		System.out.println(empleado.getTareas());
		contexto.close();
	}

}
