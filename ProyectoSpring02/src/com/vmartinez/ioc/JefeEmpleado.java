package com.vmartinez.ioc;

public class JefeEmpleado implements Empleado {

private CreacionInformes informeNuevo;
	
	//Creacion de constructor que inyecta la dependencia
	public JefeEmpleado(CreacionInformes informeNuevo) {
		this.informeNuevo = informeNuevo;
	}
	
	public  String getTareas() {
		return "Gestiono las cuestiones relativas de secci�n";
	}

	@Override
	public String getInforme() {
		// TODO Auto-generated method stub
		return "Informe de Jefe: "+informeNuevo.getInforme();
	}
}
