package com.vmartinez.ioc;

public class DirectorEmpleado implements Empleado {

	private CreacionInformes informeNuevo;
	
	//Creacion de constructor que inyecta la dependencia
	public DirectorEmpleado(CreacionInformes informeNuevo) {
		this.informeNuevo = informeNuevo;
	}

	@Override
	public String getTareas() {
		// TODO Auto-generated method stub
		return "Gestionar empresa";
	}

	@Override
	public String getInforme() {
		// TODO Auto-generated method stub
		return "Informe director"+ informeNuevo.getInforme();
	}

}
